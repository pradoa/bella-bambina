/*
 Navicat Premium Data Transfer

 Source Server         : resultyprojetos.com.br_3306
 Source Server Type    : MySQL
 Source Server Version : 50551
 Source Host           : resultyprojetos.com.br:3306
 Source Schema         : resultyp_bb

 Target Server Type    : MySQL
 Target Server Version : 50551
 File Encoding         : 65001

 Date: 24/11/2017 16:52:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for clientes
-- ----------------------------
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` bigint(12) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;

-- ----------------------------
-- Table structure for cores
-- ----------------------------
DROP TABLE IF EXISTS `cores`;
CREATE TABLE `cores` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `marca` bigint(12) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;

-- ----------------------------
-- Table structure for linhas
-- ----------------------------
DROP TABLE IF EXISTS `linhas`;
CREATE TABLE `linhas` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` bigint(12) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;

-- ----------------------------
-- Table structure for marcas
-- ----------------------------
DROP TABLE IF EXISTS `marcas`;
CREATE TABLE `marcas` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` bigint(12) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `linha` bigint(12) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;

-- ----------------------------
-- Table structure for pedidos
-- ----------------------------
DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE `pedidos` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` bigint(12) unsigned NOT NULL,
  `marca` bigint(12) unsigned NOT NULL,
  `cliente` bigint(12) unsigned NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;

-- ----------------------------
-- Table structure for pedidos_itens
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_itens`;
CREATE TABLE `pedidos_itens` (
  `id` bigint(24) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` bigint(12) NOT NULL,
  `pedido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;

-- ----------------------------
-- Table structure for pedidos_itens_qt
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_itens_qt`;
CREATE TABLE `pedidos_itens_qt` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `item` bigint(12) NOT NULL,
  `tamanho` bigint(12) NOT NULL,
  `quantidade` bigint(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;

-- ----------------------------
-- Table structure for planilha
-- ----------------------------
DROP TABLE IF EXISTS `planilha`;
CREATE TABLE `planilha` (
  `id` bigint(24) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` bigint(12) NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referencia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tamanho` varchar(255
<?php

require_once('master/header.php');

?>

<body class="login-page">
	<div class="login-container">
		<div class="login-branding">
			<a href="index.html"><img src="images/logo.png" alt="Mouldifi" title="Mouldifi"></a>
		</div>
		<div class="login-content">
			<h2><strong>Bem-vindo(a)!</strong> Por favor, faça login:</h2>
			<form class="form" action="dologin.php" success="core.showNotification(core.requestData.msg, core.requestData.icon, core.requestData.type);if (core.requestData.type == 'success'){setTimeout(function(){core.loadPage('index.php');}, 2000);}" fail="core.showNotification('Não foi possível fazer login. (Código 1)', 'error', 'danger');" rest>
				<div class="form-group">
					<input type="text" name="user" placeholder="Username" class="form-control">
				</div>                        
				<div class="form-group">
					<input type="password" name="password" placeholder="Password" class="form-control">
				</div>
				<div class="form-group">
					<a href="#" submit><button class="btn btn-primary btn-block">Login</button></a>
				</div>
				<p class="text-center"><a href="forgot-password.html">Esqueceu sua senha?</a></p>                        
			</form>
		</div>
	</div>
	
	<!--Load JQuery-->
	<script src="js/jquery.min.js"></script>
	<script src="js/core.js"></script>
</body>
</html>

<?php

require_once('../lib/core.php');
header('Content-type: application/json');

$type = __($_GET['type']);
$col = __($_GET['col']);
$val = __($_GET['val']);
$op = __($_GET['op']);

$usr = $_SESSION[SERVER_IDENT]['id'];

switch ($type){

    case "valores":{
        $ref = __($_GET['ref']);
        # Adaptar região para regiao do usuário
        $res = getAll($type . " WHERE `referencia`='$ref' AND `regiao`='1' ");
        break;
    }
    
    case "clientes":{
        $res = getAll($type . " WHERE `usuario`='$usr' ");
        break;
    }
    
    case "pedidos":{
        $data = array();

        $res = getAll($type . " WHERE `usuario`='$usr' ");
        foreach ($res as $row){
            $row['marca'] = getName('marcas', $row['marca']);
            $row['cliente'] = getName('clientes', $row['cliente']);

            $row['actions'] = '<a href="show-order.php?id='.$row['id'].'"><button type="button" class="btn btn-primary btn-sm icon">
            <i class="material-icons">exit_to_app</i> Ver Pedido
        </button></a>';

            array_push($data, $row);
        }

        $res = $data;

        break;
    }
    
    case "marcas":{
        $data = array();

        $res = getAll($type . "");
        foreach ($res as $row){
            $row['linha'] = getName('linhas', $row['linha']);
            $row['actions'] = '<a href="javascript:void(0);" data-toggle="edit" data-id="'.$row['id'].'"><button type="button" class="btn btn-primary btn-sm icon"><i class="material-icons">edit</i> Editar Marca</button></a>';

            array_push($data, $row);
        }

        $res = $data;

        break;
    }
    
    case "marca":{
        $data = array();
        $res = getAll("marcas WHERE `$col` $op '$val'");

        break;
    }
    
    case "linhas":{
        $data = array();

        $res = getAll($type . "");
        
        foreach ($res as $row){

            $row['marca'] = getName('marcas', $row['marca']);

            $row['actions'] = '<a href="lines-edit.php?id='.$row['id'].'"><button type="button" class="btn btn-primary btn-sm icon"><i class="material-icons">edit</i> Editar Grupo</button></a>';

            array_push($data, $row);
        }

        $res = $data;

        break;
    }
    
    case "tamanhos":{
        $data = array();
        $res = getAll($type . " WHERE `$col` $op '$val'");
        
        foreach ($res as $row){

            $row['actions'] = '<a href="javascript:void(0);" data-id="'.$row['id'].'"><button type="button" class="btn btn-primary btn-sm icon"><i class="material-icons">edit</i> Editar</button></a>&nbsp;&nbsp;<a href="javascript:void(0);" data-id="'.$row['id'].'"><button type="button" class="btn btn-danger btn-sm icon"><i class="material-icons">delete</i> Remover</button></a>';

            array_push($data, $row);
        }

        $res = $data;

        break;
    }


    
    case "produtos":{
        $data = array();

        $sql = $type . "";

        if ($col != ""){
            if ($op == ''){
                $op = '=';
            }
    
            $sql .= " WHERE `$col` $op '$val'";
        }

        $res = getAll($sql);
        
        foreach ($res as $row){

            $row['marca'] = getName('marcas', $row['marca']);

            $row['actions'] = '<a href="javascript:void(0);" data-toggle="edit" data-id="'.$row['id'].'"><button type="button" class="btn btn-primary btn-sm icon"><i class="material-icons">edit</i> Editar Produto</button></a>';

            array_push($data, $row);
        }

        $res = $data;

        break;
    }

    default: {
        if ($col != ''){
            if ($op == ''){
                $op = '=';
            }
    
            $type .= " WHERE `$col` $op '$val'";
        }
    
        $res = getAll($type);
    }
    break;

}


if (!isset($res['error'])){
    echo json_encode($res);
    exit;
}

echo json_encode('{"msg": "'.$res['error'].'", "icon": "error", "type": "danger"}');

?>
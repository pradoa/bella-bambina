<?php

require_once('../lib/core.php');
header('Content-type: application/json');

$res = array();

$type = __($_POST['type']);
$col = __($_POST['col']);
$val = __($_POST['val']);
$op = __($_POST['op']);

$usr = $_SESSION[SERVER_IDENT]['id'];

switch ($type){
    
    case "pedidos": {
        $cli = __($_POST['cliente']);
        $mar = __($_POST['marca']);
        $des = __($_POST['descricao']);

        $fields = array('usuario','cliente','marca','descricao');
        $values = array($usr, $cli, $mar, $des);

        $id = post($mysqli, $type, $fields, $values);
        $res = array('id' => $id);
        break;
    }
    
    case "marcas": {

        if (isset($_POST['id'])){

            $id = __($_POST['id']);
            $des = __($_POST['descricao']);
            $lin = __($_POST['linha']); 

            $fields = array('descricao','linha');
            $values = array($des, $lin);

            $id = update($mysqli, $type, $fields, $values, $id);
            $res = array('code' => 1);

        } else {
            $mar = __($_POST['nome']); 
            $des = __($_POST['descricao']);
            $lin = __($_POST['linha']); 

            $fields = array('usuario','nome','descricao','linha');
            $values = array($usr, $mar, $des, $lin);

            $id = post($mysqli, $type, $fields, $values);
            $res = array('id' => $id);
        }
        break;
    }
        
    case "linhas": {
        $lin = __($_POST['nome']);
        $des = __($_POST['descricao']);
        $mar = __($_POST['marca']);

        $fields = array('usuario','nome','marca','descricao');
        $values = array($usr, $lin, $mar, $des);

        $id = post($mysqli, $type, $fields, $values);
        $res = array('id' => $id);
        break;
    }

    case "tamanhos": {
        $tam = __($_POST['nome']);
        $lin = __($_POST['linha']);

        $fields = array('usuario','nome','linha');
        $values = array($usr, $tam, $linha);

        $id = post($mysqli, $type, $fields, $values);
        $res = array('id' => $id);
        break;
    }

    case "produtos": {
        
        if (isset($_POST['id'])){

            $id = __($_POST['id']);
            $pro = __($_POST['nome']);
            $mar = __($_POST['marca']);

            $fields = array('nome','marca');
            $values = array($pro, $mar);

            $id = update($mysqli, $type, $fields, $values, $id);
            $res = array('code' => 1);

        } else {
            $pro = __($_POST['nome']); 
            $mar = __($_POST['marca']);
            $cod = __($_POST['ref']); 

            $fields = array('usuario','nome','ref','marca');
            $values = array($usr, $pro, $cod, $mar);

            $id = post($mysqli, $type, $fields, $values);
            $res = array('id' => $id);
        }
        break;
    }

    default: {

    }
    break;

}


if (!isset($res['error'])){
    echo json_encode($res);
    exit;
}

echo json_encode('{"msg": "'.$res['error'].'", "icon": "error", "type": "danger"}');

?>
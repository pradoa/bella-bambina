<?php

require_once('../lib/core.php');
header('Content-type: application/json');

$pedido = __($_GET['pedido']);


$res = getFieldsWhere('pedidos_itens', 'id,produto,cor,valor', 'pedido', $pedido);
$result = array();
$total_val = 0;

foreach ($res as $r){

  //$r['valor_total'] = 'R$ ';// .  str_replace('.', ',', number_format($qt * toFloat($r['valor']), 2));

  $r['produto'] = getName('produtos', $r['produto']);
  $r['cor'] = $r['cor'] > 0 ? getName('cores', $r['cor']) : "";

  $tams = getFieldsWhere('pedidos_itens_qt', 'id,tamanho,quantidade', 'item', $r['id']);

  $val = 0;
  foreach ($tams as $tam){
    $qt = $tam['quantidade'];
    $val += $qt * toFloat($r['valor']);

    $total_val += $val;

    $r['tamanho' . $tam['tamanho']] = $qt;
  }

  $r['valor_total'] = 'R$ ' . str_replace(':', ',', str_replace(',', '.', str_replace('.', ':', number_format($val, 2))));

  array_push($result, $r);

}

echo json_encode($result);

?>
<?php

require_once('../lib/core.php');
header('Content-type: application/json');

$user = '';
$pass = '';

if (isset($_POST['user']))
    $user = $_POST['user'];
if (isset($_POST['password']))
    $pass = $_POST['password'];

$res = doLogin($mysqli, $user, $pass);
if ($res == "LOGIN_OK"){
    echo json_encode('{"msg": "Login efetuado com sucesso!", "icon": "done", "type": "success"}');  
    exit;
}

echo json_encode('{"msg": "'.$res.'", "icon": "error", "type": "danger"}');

?>
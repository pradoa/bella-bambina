
    <!-- Page sidebar -->
    <div class="page-sidebar">
    
      <!-- Site header  -->
      <header class="site-header">
        <div class="site-logo"><a href="index.html"><img src="images/logo.png" alt="Bella Bambina" title="Bella Bambina"></a></div>
        <div class="sidebar-collapse hidden-xs"><a class="sidebar-collapse-icon" href="#"><i class="icon-menu"></i></a></div>
        <div class="sidebar-mobile-menu visible-xs"><a data-target="#side-nav" data-toggle="collapse" class="mobile-menu-icon" href="#"><i class="icon-menu"></i></a></div>
      </header>
      <!-- /site header -->
      
      <!-- Main navigation -->
      <ul id="side-nav" class="main-menu navbar-collapse collapse">
        <li>
          <a href="index.php">
            <i class="material-icons">dashboard</i><span class="title">Dashboard</span>
          </a>
        </li>
        <li>
          <a href="orders.php">
            <i class="material-icons">grid_on</i><span class="title">Pedidos</span>
          </a>
        </li>
        <li>
          <a href="products.php">
            <i class="material-icons">list</i><span class="title">Produtos</span>
          </a>
        </li>
        <li>
          <a href="brands.php">
            <i class="material-icons">list</i><span class="title">Marcas</span>
          </a>
        </li>
        <li>
          <a href="lines.php">
            <i class="material-icons">list</i><span class="title">Tamanhos</span>
          </a>
        </li>
      </ul>
      <!-- /main navigation -->		
    </div>
  <!-- /page sidebar -->
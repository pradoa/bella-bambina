<?php

require_once('lib/core.php');

?><!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
	<link rel="icon" type="image/png" href="img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Bella Bambina</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	
	<link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
	<link href="css/entypo.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mouldifi-core.css" rel="stylesheet">
	<link href="css/mouldifi-forms.css" rel="stylesheet" >

	<link href="css/plugins/datatables/jquery.dataTables.css" rel="stylesheet">
	<link href="js/plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">
	<link href="css/plugins/select2/select2.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
	<![endif]-->
</head>
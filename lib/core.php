<?php

# System Definitions
define ('SERVER_IDENT', 'com.resulty.projetos.bella.bambina');
define ('SESSION_DURA', 3600);
define ('SERVER_PATH', 'http://resultyprojetos.com.br/bella-bambina/');

# Start Session
session_start();
function valid_session(){
    session_id() == "";
}

# Support Contact Info
define ('ADMIN_MAIL', 'adrian.prado@tvtem.com');
define ('ADMIN_NAME', 'Adrian Prado');
define ('ADMIN_FONE', '(15) 99633-1299');
define ('ADMIN_TITLE', 'Analista de Desenvolvimento Web');
define ('ADMIN_MSG',  '<br /><br /><br />Please contact the admin of the service.<hr><div><table class="MsoNormalTable" border="0" cellpadding="0"><tbody><tr><td style="padding: 0.75pt;"><p class="MsoNormal"><span class="object"><span style="color: rgb(51, 102, 153);"><span class="Object" role="link" id="OBJ_PREFIX_DWT2223_com_zimbra_url"><span class="Object" role="link" id="OBJ_PREFIX_DWT2259_com_zimbra_url"><a href="http://g1.globo.com/sao-paulo/sorocaba-jundiai/index.html" target="_blank"><span style="color: white; border: 1pt none windowtext; padding: 0cm; text-decoration: none;"><img border="0" id="_x0000_i1025" dfsrc="http://temmais.com/temp/assinatura/2015/logo_rede.jpg" src="http://temmais.com/temp/assinatura/2015/logo_rede.jpg" saveddisplaymode=""></span></a></span></span></span></span></p></td><td style="padding: 0.75pt;"><table class="MsoNormalTable" border="0" cellpadding="0"><tbody><tr><td style="padding: 0.75pt;"><div><p class="MsoNormal"><b><span style="font-size: 15pt; font-family: arial, sans-serif; color: rgb(105, 106, 107);">Adrian&nbsp;Fernandes</span></b><br><span style="font-size: 10.5pt;font-family: arial, sans-serif; color: rgb(105, 106, 107);">adrian.prado@tvtem.com</span><br><span style="font-size: 10.5pt; font-family: arial, sans-serif; color: rgb(105, 106, 107);">Analista de Tecnologia Web | Sorocaba</span><br><span style="font-size: 10.5pt; font-family: arial, sans-serif; color: rgb(105, 106, 107);">15. &nbsp; 3224-8774</span><br /><span style="font-size: 10.5pt; font-family: arial, sans-serif; color: rgb(105, 106, 107);">15.&nbsp;99633-1299</span></p></div></td></tr></tbody></table></td></tr></tbody></table></div>');

# Database Configuration
define ('DB_HOST', 'localhost');
define ('DB_USER', 'resultyp_root');
define ('DB_PASS', '123mudar');
define ('DB_BASE', 'resultyp_bb');

# Connect to mssql server
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_BASE);
$GLOBALS['mysqli'] = $mysqli;

function __($data){
    return mysqli_real_escape_string($GLOBALS['mysqli'], $data);
}

if(mysqli_connect_errno()) {
     kill("Connection could not be established.", print_r( mysqli_connect_error(), true));
}

# Login/Session Functions

function doLogout(){

    unset($_SESSION[SERVER_IDENT]);
    session_destroy();
    return 'LOGOUT_OK';
}

function doLogin($mysqli, $user, $pass){

    if (!$result = $mysqli->query("SELECT * FROM usuarios WHERE usuario='$user' and senha='$pass'")) {
        kill("Could not complete query request.", print_r( $mysqli->error, true));
    } else {
        if ($row = $result->fetch_object()) {
            registerLogin((array)$row);
            return 'LOGIN_OK';
        }

    }

    # User and Password doesn't match
    return 'Usuário ou Senha inválido.';

}

function registerLogin($data){
    # Register Session
    $_SESSION[SERVER_IDENT] = array(
        'id'        => $data['id'],
        'user'      => $data['usuario'],
        'email'     => $data['email'],
        'name'      => $data['nome'],
        'update'    => time()
    );
}

function validateLogin(){
    $update = $_SESSION[SERVER_IDENT]['update'];
    if ((time() - SESSION_DURA) > $update){

        expireSession();
        return false;
    }

    $_SESSION[SERVER_IDENT]['update'] = time();
    return true;
}

function expireSession(){
    try
    {
        unset($_SESSION[SERVER_IDENT]);

        if (valid_session())
            session_destroy();
        //header('Location: '.SERVER_PATH.'/login.php');
    } catch (Exception $e) {

    }
    return false;
}

function LoginOk(){
    $data = $_SESSION[SERVER_IDENT];

    if (isset($data['user'])){
        if (validateLogin()){
            return true;
        }
    }
    
    return expireSession();
}

function getUserID(){
    return $_SESSION[SERVER_IDENT]['id'];
}

function validateAccess($redir = null){
    if (LoginOk()){
        # TODO: check access level (admin, manager...)
        return true;
    } else{
        if ($redir != null){
            header('Location: '.$redir);
        }

        print_r('Login inválido ou expirado.');
        return false;

    }
}

# Data functions
function getDataById($mysqli, $obj, $id){
    $qry = "SELECT * FROM $obj WHERE id=$id";
    try{
        if ($obj == ''){
            throw new Exception('Invalid SQL');
        }
        $res = array();

        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            if ($row = $result->fetch_array()) {
                $res = $row;
            }
        }
        return $res;
    } catch (Exception $ex){
        return array('error' => $ex . '::('.$qry.')');
    }
}

function getAll($obj){
    $mysqli = $GLOBALS['mysqli'];
    $qry = "SELECT * FROM $obj";
    try{
        if ($obj == ''){
            throw new Exception('Invalid SQL');
        }
        $res = array();

        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            while ($row = $result->fetch_array()) {
                array_push($res, $row);
            }
        }
        return $res;
    } catch (Exception $ex){
        return array('error' => $ex . '::('.$qry.')');
    }
}
function getAllWhere($obj, $field, $value){
    $mysqli = $GLOBALS['mysqli'];
    $qry = "SELECT * FROM $obj WHERE $field='$value'";
    try{
        if ($obj == ''){
            throw new Exception('Invalid SQL');
        }
        $res = array();

        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            while ($row = $result->fetch_array()) {
                array_push($res, $row);
            }
        }
        return $res;
    } catch (Exception $ex){
        return array('error' => $ex . '::('.$qry.')');
    }
}

function getFields($mysqli, $obj, $fields){
    $qry = "SELECT $fields FROM $obj";
    try{
        if ($obj == ''){
            throw new Exception('Invalid SQL');
        }
        $res = array();

        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            while ($row = $result->fetch_array()) {
                array_push($res, $row);
            }
        }
        return $res;
    } catch (Exception $ex){
        return array('error' => $ex . '::('.$qry.')');
    }
}

function getFieldsWhere($obj, $fields, $col, $val){
    $mysqli = $GLOBALS['mysqli'];
    $qry = "SELECT $fields FROM $obj WHERE $col='$val'";
    try{
        if ($obj == ''){
            throw new Exception('Invalid SQL');
        }
        $res = array();

        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            while ($row = $result->fetch_array()) {
                array_push($res, $row);
            }
        }
        return $res;
    } catch (Exception $ex){
        return array('error' => $ex . '::('.$qry.')');
    }
}

function getName($area, $id){
    $mysqli = $GLOBALS['mysqli'];
    $qry = "SELECT nome FROM $area WHERE id='$id'";
    try{
        if ($area == '' || $id == ''){
            throw new Exception('Invalid SQL');
        }
        $res = array();

        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            while ($row = $result->fetch_array()) {
                array_push($res, $row[0]);
            }
        }
        return $res[0];
    } catch (Exception $ex){
        return array('error' => $ex . '::('.$qry.')');
    }
}

function getTitle($mysqli, $area, $id){
    return getName($area, $id);
}

function post($mysqli, $obj, $fields, $values) {
    $qry = "INSERT INTO $obj ({0}) VALUES ({1})";
    try {
        if ($obj == '') {
            throw new Exception('Invalid SQL');
        }

        $f = implode(',', $fields);
        $v = "'". implode("','", $values) ."'";

        $qry = str_replace('{0}', $f, $qry);
        $qry = str_replace('{1}', $v, $qry);

        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            return $mysqli->insert_id;
        }
    } catch (Exception $ex) {
        return array('error' => $ex . '::('.$qry.')');
    }
}



function update($mysqli, $obj, $fields, $values, $id) {
    $qry = "UPDATE $obj SET ";
    try {
        if ($obj == '' || sizeof($fields) != sizeof($values)) {
            throw new Exception('Invalid SQL');
        }

        for ($i = 0; $i < sizeof($fields); $i++){
            if ($i > 0)
                $qry .= ',';
            
            $qry .= $fields[$i] . "='".$values[$i]."'";
        }

        $qry .= " WHERE id='".$id."'";

        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            return $mysqli->insert_id;
        }
    } catch (Exception $ex) {
        return array('error' => $ex . '::('.$qry.')');
    }
}

# System functions
function kill($msg, $data){
    echo $msg . '<br />';

    die ('<pre>'.$data.'</pre>');
}

function toFloat($num){
    $num = str_replace('R$ ', '', $num);
    $num = str_replace(',', '.', $num);

    return $num;
}


# Template Functions
function prepareTemplate($mainTemplate = 'index-page'){
    printf('
    <script class="remove-at-load">
        document.addEventListener("DOMContentLoaded", function() {
            var script = document.createElement("script");
            script.setAttribute("class", "remove-at-load" );
            script.innerHTML = "$(\'body\').removeClass(\'index-page\').addClass(\''.$mainTemplate.'\');$(\'.remove-at-load\').remove();";
            document.body.appendChild(script);
        });
    </script>
    ');

}


?>
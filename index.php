<?php

require_once('lib/core.php');
validateAccess('login.php');


header('Location: orders.php');
require_once('master/header.php');

?>

<body>
  <div class="page-container">

  <?php include('master/sidebar.php'); ?>

    <div class="main-container">


    <?php include('master/userbar.php'); ?>

    
	
	 <!-- Main content -->
	 <div class="main-content">
			<h1 class="page-title">Produtos</h1>
			<!-- Breadcrumb -->
			<ol class="breadcrumb breadcrumb-2"> 
				<li><a href="index.php"><i class="fa fa-home"></i>Home</a></li> 
				<li class="active"><strong>Planilha</strong></li> 
			</ol>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
							<h3 class="panel-title">Planilha de Produtos</h3>
							<ul class="panel-tool-options"> 
								<li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="items" class="table table-striped table-bordered table-hover dataTables-example" >
									<thead>
										<tr>
											<th>Marca</th>
											<th>Referência</th>
											<th>Cor</th>
											<th>Tamanho</th>
											<th>Qtd.</th>
											<th>Valor Unit.</th>
											<th>Valor Total</th>
										</tr>
									</thead>
									<tbody id="tableContent">
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td id="totalVal">R$ 00,00</td>
										</tr>
										<tr>
											<th id="marWrapper">
											</th>
											<th id="refWrapper">
											</th>
											<th id="corWrapper">
											</th>
											<th id="tamWrapper">
											</th>
											<th id="qtdWrapper">
											</th>
											<th id="valWrapper">
											</th>
											<th><button id="addItem" class="btn btn-primary btn-block" type="button" >Adicionar</button></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		
	  </div>
	  <!-- /main content -->
  

    </div>
  </div>

<?php

require_once('master/footer.php');

?>

<script src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="js/plugins/datatables/jszip.min.js"></script>
<script src="js/plugins/datatables/pdfmake.min.js"></script>
<script src="js/plugins/datatables/vfs_fonts.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
	$(document).ready(function () {

		var $valor = 0; // initial value
		function updateVal($el, $val){
			//$val = parseInt($val * 100);
			var $v = $val.formatMoney(2, '.', ',');
			$el.html('R$ ' + $v);
		}

		$.get('rest/gettable.php', function(data){
			var $opts = {
					data: data,
					columns: [
						{ data: "marca" },
						{ data: "referencia" },
						{ data: "cor" },
						{ data: "tamanho" },
						{ data: "quantidade" },
						{ data: "valor" },
						{ data: "valor_total" }
					]
				};
			var $items = $('#items').DataTable($opts);
		});

		var $marcas = $('<select id="marca" name="marca" class="select2 form-control" data-placeholder="Marca"><option value="">Marca</option></select>');

		$.get('rest/getdata.php?type=marcas', function(data){

			$(data).each(function(){
				var $r = JSON.parse(JSON.stringify($(this)));
				var $obj = $r[0];

				$opt = $('<option value="'+$obj.id+'">'+$obj.nome+'</option>');
				$marcas.append($opt);
			});


			$("#marWrapper").html('').append($marcas);

			$("#marca").select2()
				.change(function(){
					var $val = $(this).val();

					var $ref = $('<select id="ref" name="ref" class="select2 form-control" data-placeholder="Referência"></select>');
					var $tam = $('<div id="tamanhos"></div>');
					var $qtd = $('<div id="quantidades"></div>');
					var $cor = $('<select id="cor" name="cor" class="select2 form-control" data-placeholder="Cor"></select>');

					$.get('rest/getdata.php?type=referencias&col=marca&val='+$val, function(data){
						$(data).each(function(){
							var $r = JSON.parse(JSON.stringify($(this)));
							var $obj = $r[0];

							$opt = $('<option value="'+$obj.id+'">'+$obj.nome+'</option>');
							$ref.append($opt);
						});

						$('#refWrapper').html('').append($ref);

						$ref.select2().change(function(){
							var $_val = $(this).val();

							$.get('rest/getdata.php?type=valores&ref='+$_val, function(data){
								$(data).each(function(){
									var $r = JSON.parse(JSON.stringify($(this)));
									var $obj = $r[0];

									$eval = $('<div class="tamBox" id="val-'+$obj.id+'">R$ 0,00</div>');
									$('#valWrapper').html($eval);
									updateVal($eval, parseFloat($obj.valor));
								});
							});

						}).change();

					});

					$.get('rest/getdata.php?type=tamanhos&col=marca&val='+$val, function(data){
						$(data).each(function(){
							var $r = JSON.parse(JSON.stringify($(this)));
							var $obj = $r[0];

							$opt = $('<input class="form-control qt-input" id="qt-'+$obj.id+'" name="qt-'+$obj.id+'"  data-mask="9?99" placeholder="Quantidade '+$obj.nome+'"/>');
							$qtd.append($opt);
							$tam.append($('<div class="tamBox">'+$obj.nome+'</div>'));
						});

						$('#tamWrapper').html('').append($tam);
						$('#qtdWrapper').html('').append($qtd);
					});


					$.get('rest/getdata.php?type=cores&col=marca&val='+$val, function(data){
						$(data).each(function(){
							var $r = JSON.parse(JSON.stringify($(this)));
							var $obj = $r[0];

							$opt = $('<option value="'+$obj.id+'">'+$obj.nome+'</option>');
							$cor.append($opt);
						});

						$('#corWrapper').html('').append($cor);
						$cor.select2();
					});

				});

		});

		$('#addItem').click(function(){
			$marca = $('#marca').val();
			$ref = $('#ref').val();
			
			$tams = [];
			$('#tamanhos').find('.tamBox').each(function(){
				$tams.push($(this).html());
			});

			$cor = $('#cor').val();

			$qtds = [];
			$qtd = $('#qtdWrapper').find('input').each(function(){
				$qtds.push($(this).val());
			});

			var formData = [];
			formData['marca'] = $marca;
			formData['referencia'] = $ref;
			formData['tamanhos'] = $tams;
			formData['cor'] = $cor;
			formData['quantidades'] = $qtds;
			formData['valor'] = $('#valWrapper').find('div').html();

			var $data = $.extend({}, formData);

			$.post( "rest/posttable.php", $data)
			.done(function( data ) {
				console.log(data);
			});

			$.get('rest/gettable.php', function(data){
				var $opts = {
					  destroy: true,
						data: data,
						columns: [
							{ data: "marca" },
							{ data: "referencia" },
							{ data: "cor" },
							{ data: "tamanho" },
							{ data: "quantidade" },
							{ data: "valor" },
							{ data: "valor_total" }
						]
					};
				var $items = $('#items').DataTable($opts);
			});
			return false;
		});
	});
</script>


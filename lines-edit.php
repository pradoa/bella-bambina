<?php

require_once('lib/core.php');
validateAccess('login.php');

require_once('master/header.php');

$id = __($_GET['id']);
$name = getName('linhas', $id);

?>

<body>
  <div class="page-container">

  <?php include('master/sidebar.php'); ?>

    <div class="main-container">


    <?php include('master/userbar.php'); ?>

    
	
	 <!-- Main content -->
	 <div class="main-content">
	 		<div class="pull-right"><a href="lines.php"><button type="button" class="btn btn-primary icon"><i class="material-icons">arrow_back</i> Voltar</button></a></div>
			<h1 class="page-title">Linha: <?=$name?></h1>
			<!-- Breadcrumb -->
			<ol class="breadcrumb breadcrumb-2"> 
        <li><a href="index.php"><i class="fa fa-home"></i>Home</a></li> 
				<li><a href="linhas.php"><i class="fa fa-home"></i>Linhas Primeiros Passos</a></li> 
				<li class="active"><strong><?=$name?></strong></li> 
			</ol>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
							<h3 class="panel-title"><?=$name?></h3>
							<div class="pull-right">
								<button id="newOrderShow" type="button" class="btn btn-primary btn-md icon">
									<i class="material-icons">add</i> Adicionar Tamanho
								</button>
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="items" class="table table-striped table-bordered table-hover dataTables-example" >
									<thead>
										<tr>
											<th>id</th>
											<th>Tamanho</th>
											<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
										</tr>
									</thead>
									<tbody id="tableContent">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		
	  </div>
	  <!-- /main content -->
  
		<div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<form id="newOrder">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Adicionar Tamanho</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12">
                  <div  class="form-group">
										<input type="hidden" name="linha" value="<?=$id?>" />
                    <input type="text" class="form-control" name="nome" placeholder="Tamanho" />
                  </div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button id="fazPedido" type="button" class="btn btn-primary">Adicionar Novo Tamanho</button>
						</div>
					</div>
				</div>
			</form>
		</div>

    </div>
  </div>

<?php

require_once('master/footer.php');

?>

<script src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="js/plugins/datatables/jszip.min.js"></script>
<script src="js/plugins/datatables/pdfmake.min.js"></script>
<script src="js/plugins/datatables/vfs_fonts.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
	$(document).ready(function () {

		$('#newOrderShow').click(function(){
			$('#newModal').modal('show');
		});

		$('#fazPedido').click(function(){
			var $form = $('#newOrder');
			var $data =  $form.serialize() + "&type=tamanhos";

			$.post(
				'rest/postdata.php', 
				$data,
				function(res){
					var id = res.id;
					location.reload(true);
				}
			);
		});


		$.get('rest/getdata.php?type=tamanhos&col=linha&op=%3D&val=<?=$id?>', function(data){
			var $opts = {
					data: data,
					columns: [
						{ data: "id" },
						{ data: "nome" },
						{ data: "actions" }
					]
				};
			var $items = $('#items').DataTable($opts);
		});

	});
</script>


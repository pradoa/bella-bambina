<?php

require_once('lib/core.php');
validateAccess('login.php');

require_once('master/header.php');

$id = __($_GET['id']);
$orderData = getDataById($mysqli, 'pedidos', $id);
$marca = $orderData['marca'];
$marcaData = getDataById($mysqli, 'marcas', $marca);
$linha = $marcaData['linha'];

$title = "Pedido Nº ".$orderData['id'];
$brand = getName('marcas', $orderData['marca']);
$sizes = getAllWhere('tamanhos', 'linha', $linha);


?>
<body>
  <div class="page-container">

  <?php include('master/sidebar.php'); ?>

    <div class="main-container">


    <?php include('master/userbar.php'); ?>

    
	
	 <!-- Main content -->
	 <div class="main-content">
			<h1 class="page-title"><?=$title?></h1>
			<!-- Breadcrumb -->
			<ol class="breadcrumb breadcrumb-2"> 
			<li><a href="index.php"><i class="fa fa-home"></i>Home</a></li> 
				<li><a href="orders.php">Pedidos</a></li> 
				<li class="active"><strong><?=$title?></strong></li> 
			</ol>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
							<h3 class="panel-title"><?=$brand?></h3>
							<ul class="panel-tool-options"> 
								<li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="items" class="table table-striped table-bordered table-hover dataTables-example" >
									<thead>
										<tr>
											<th>Cód. Referência</th>
											<th>Cor</th>
											<?
												foreach ($sizes as $size){
													echo '<th>'.getName('tamanhos', $size['id']).'</th>';
												}
											?>
											<th>Valor Unit.</th>
											<th>Valor Total</th>
										</tr>
									</thead>
									<tbody id="tableContent">
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<?
												foreach ($sizes as $size){
													echo '<th></th>';
												}
											?>
											<td></td>
											<td id="totalVal">R$ 00,00</td>
										</tr>
										<tr>
											<th id="refWrapper">
											</th>
											<th id="corWrapper">
											</th>
											<?
												foreach ($sizes as $size){
													echo '<th class="tamWrapper"><input class="form-control qt" data-mask="9?999" name="'.$size['id'].'" placeholder="Qtd." /></th>';
												}
											?>
											<th id="valWrapper">
											</th>
											<th><button id="addItem" class="btn btn-primary btn-block" type="button" >Adicionar</button></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		
	  </div>
	  <!-- /main content -->
  

    </div>
  </div>

<?php

require_once('master/footer.php');

?>

<script src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="js/plugins/datatables/jszip.min.js"></script>
<script src="js/plugins/datatables/pdfmake.min.js"></script>
<script src="js/plugins/datatables/vfs_fonts.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
	$(document).ready(function () {

		var $items = 0;
		loadTable = function(){

			
					$.get('rest/gettable.php?pedido=<?=$orderData['id']?>',
						function(data){
							var $total_val = 0;
						$.each(data, function(){
							var $add = parseFloat(this.valor_total.replace('R$', '').replace('.', ''));
							$total_val += $add;
							console.log('Current Val: ' + $total_val);
							updateVal($('#totalVal'), $total_val);
						})
						var $opts = {
								data: data,
								columns: [
									{ data: "produto" },
									{ data: "cor" },
									<?
										foreach ($sizes as $size){
											echo '{ data: "tamanho'.$size['id'].'" },';
										}
									?>
									{ data: "valor" },
									{ data: "valor_total" }
								]
							};

						if ($items != 0){
							$items.clear();
							$items.destroy();
						}
						$items = $('#items').DataTable($opts);
					});
		}

		function updateVal($el, $val){
			//$val = parseInt($val * 100);
			var $v = $val.formatMoney(2, '.', ',');
			$el.html('R$ ' + $v);
		}


		var $ref = $('<select id="ref" name="ref" class="select2 form-control" data-placeholder="Referência"></select>');
		var $cor = $('<select id="cor" name="cor" class="select2 form-control" data-placeholder="Cor"></select>');

		$.get('rest/getdata.php?type=produtos&col=marca&val=<?=$orderData['marca']?>', function(data){
			$(data).each(function(){
				var $r = JSON.parse(JSON.stringify($(this)));
				var $obj = $r[0];

				$opt = $('<option value="'+$obj.id+'">'+$obj.ref+' ('+$obj.nome+')</option>');
				$ref.append($opt);
			});

			$('#refWrapper').html('').append($ref);

			$ref.select2().change(function(){
				var $_val = $(this).val();

				$.get('rest/getdata.php?type=valores&ref=<?=$orderData['marca']?>', function(data){
					$(data).each(function(){
						var $r = JSON.parse(JSON.stringify($(this)));
						var $obj = $r[0];

						$eval = $('<div class="tamBox" id="val-'+$obj.id+'">R$ 0,00</div>');
						$('#valWrapper').html($eval);
						updateVal($eval, parseFloat($obj.valor));
					});
				});

			}).change();

		});


		$.get('rest/getdata.php?type=cores&col=marca&val=<?=$orderData['marca']?>', function(data){
			$(data).each(function(){
				var $r = JSON.parse(JSON.stringify($(this)));
				var $obj = $r[0];

				$opt = $('<option value="'+$obj.id+'">'+$obj.nome+'</option>');
				$cor.append($opt);
			});

			$('#corWrapper').html('').append($cor);
			$cor.select2();
		});


		$('#addItem').click(function(){
			$ref = $('#ref').val();
			
			$tams = [];
			$('.qt').each(function(){
				var $el = $(this);
				$tams.push($(this).attr('name') + '->' + $el.val());
			});

			$cor = $('#cor').val();

			var formData = [];
			formData['pedido'] = '<?=$orderData['id']?>';
			formData['produto'] = $ref;
			formData['cor'] = $cor;
			formData['quantidades'] = $tams;
			formData['valor'] = $('#valWrapper').find('div').html();

			var $data = $.extend({}, formData);
				//console.log($data);
			$.post( "rest/posttable.php", $data)
			.done(function( data ) {
				//console.log(data);
				loadTable();
			});

			loadTable();

			return false;
		});

		loadTable();
	});
</script>


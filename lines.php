<?php

require_once('lib/core.php');
validateAccess('login.php');

require_once('master/header.php');

?>

<body>
  <div class="page-container">

  <?php include('master/sidebar.php'); ?>

    <div class="main-container">


    <?php include('master/userbar.php'); ?>

    
	
	 <!-- Main content -->
	 <div class="main-content">
			<h1 class="page-title">Grupos de Tamanhos</h1>
			<!-- Breadcrumb -->
			<ol class="breadcrumb breadcrumb-2"> 
				<li><a href="index.php"><i class="fa fa-home"></i>Home</a></li> 
				<li class="active"><strong>Grupos de Tamanhos</strong></li> 
			</ol>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
							<h3 class="panel-title">Todas os Grupos de Tamanhos</h3>
							<div class="pull-right">
								<button id="newOrderShow" type="button" class="btn btn-primary btn-md icon">
									<i class="material-icons">add</i> Criar Grupo
								</button>
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table id="items" class="table table-striped table-bordered table-hover dataTables-example" >
									<thead>
										<tr>
											<th>id</th>
											<th>Nome</th>
											<th>Descrição</th>
											<th></th>
										</tr>
									</thead>
									<tbody id="tableContent">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		
	  </div>
	  <!-- /main content -->
  
		<div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<form id="newOrder">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Novo Grupo</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12">
                  <div  class="form-group">
                    <input type="text" class="form-control" name="nome" placeholder="Nome do Grupo" />
                  </div>
								</div>
								<div class="col-xs-12">
									<div  class="form-group">
										<input type="text" class="form-control" name="descricao" placeholder="Descrição" />
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button id="fazPedido" type="button" class="btn btn-primary">Adicionar Novo Grupo</button>
						</div>
					</div>
				</div>
			</form>
		</div>

    </div>
  </div>

<?php

require_once('master/footer.php');

?>

<script src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="js/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="js/plugins/datatables/jszip.min.js"></script>
<script src="js/plugins/datatables/pdfmake.min.js"></script>
<script src="js/plugins/datatables/vfs_fonts.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
	$(document).ready(function () {

		$('#newOrderShow').click(function(){
			$('#newModal').modal('show');
		});

		$('#fazPedido').click(function(){
			var $form = $('#newOrder');
			var $data =  $form.serialize() + "&type=linhas";

			$.post(
				'rest/postdata.php', 
				$data,
				function(res){
					var id = res.id;
					location.href = "lines-edit.php?id=" + id;
				}
			);
		});


		$.get('rest/getdata.php?type=linhas', function(data){
			var $opts = {
					data: data,
					columns: [
						{ data: "id" },
						{ data: "nome" },
						{ data: "descricao" },
						{ data: "actions" }
					]
				};
			var $items = $('#items').DataTable($opts);
		});

	});
</script>

